package browser;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class IE11 {
	
	public static WebDriver driver;
	public static WebDriver getDriver()
	{
		if(driver!=null)
		{
			return driver;
		}
		else
		{
			openBrowser("chrome");
			{				
				return driver;
				
			}
		}
	}
	
	public static void setDriver(WebDriver driver)
	{
		IE11.driver=driver;
	}
	
	public static void openBrowser(String browser)
	{
		if(browser.equalsIgnoreCase("chrome"))
		{
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();
			driver.manage().window().maximize();
		}
	}
	
	public static void openUrlBrowser(String URL)
	{
		if(driver!=null)
		{
			driver.get(URL);
		}
	}
	
	public static void ImplicitWaitforSecond(long sec)
	{
		driver.manage().timeouts().implicitlyWait(sec, TimeUnit.SECONDS);
	}
	
	public static void GoogleSearch(String Searchword)
	{
		driver.findElement(By.xpath("//input[@name='q']")).sendKeys(Searchword);
	}
	public static void Clicksearch()
	{
	driver.findElement(By.xpath("(//input[@name='btnK'])[2]")).click();
	}
	
	
	
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
////		ChromeDriver driver = new ChromeDriver();
//		WebDriverManager.iedriver().setup();
//		driver = new InternetExplorerDriver();
//		driver.manage().window().maximize();
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		driver.get("https://www.google.com");
//		driver.findElement(By.xpath("//input[@name='q']")).sendKeys("dhannesh");
	}


